package train;

public class Suica {
	private int balance; //残高
	private boolean isInsideOfGate = false; //改札内部にいるかどうか
	private Station station; //乗車駅
	
	public Suica(int balance) {
		this.balance = balance;
	}
	
	/**
	 * Suicaで運賃を支払う
	 * 残高が不足していると運賃は引かれず、falseを返す
	 * @param price
	 * @return
	 */
	public boolean pay(int price) {
		if(balance > price) {
			balance -= price;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isInsideOfGate() {
		return isInsideOfGate;
	}
	
	public void setInsideOfGate(boolean b) {
		this.isInsideOfGate = b;
	}
	
	/**
	 * 乗車駅を返す
	 * @return
	 */
	public Station getStation() {
		return station;
	}
	
	/**
	 * 乗車駅を設定する
	 * @param station
	 */
	public void setStation(Station station) {
		this.station = station;
	}
	
	public int getBalance() {
		return balance;
	}
}
