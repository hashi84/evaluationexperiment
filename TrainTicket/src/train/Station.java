package train;

/**
 * 駅のクラス
 * @author Hashimoto
 *
 */
public class Station {
	public static final int HAMAMATSU = 0;
	public static final int KAKEGAWA = 1;
	public static final int SHIZUOKA = 2;
	
	private int place;
	
	public Station(int place) {
		this.place = place;
	}
	
	public int getPlace() {
		return place;
	}

	/**
	 * 切符を改札に通して電車に乗ってもらう
	 * 12歳以上が子供料金で乗ろうとするとfalseを返す
	 * 一度改札を通した切符を使おうとするとfalseを返す
	 * @param customer
	 * @param ticket
	 * @return
	 */
	public boolean acceptTicket1(Customer customer, Ticket ticket) {
		if(ticket.isInsideOfGate()) {
			return false;
		}
		
		if(customer.getAge() < 12 && ticket instanceof ChildTicket) {
			ticket.setStation(this);
			return true;
		} else if(customer.getAge() >= 12 && !(ticket instanceof ChildTicket)) {
			ticket.setStation(this);
			return true;
		} else if(customer.getAge() < 12 && ticket instanceof ChildTicket) {
			ticket.setStation(this);
			return true;
		} else if(customer.getAge() >= 12 && ticket instanceof ChildTicket) {
			return false;
		}
		
		ticket.setStation(this);
		return true;
	}
	
	/**
	 * Suicaを改札に通して電車に乗ってもらう
	 * Suicaの不正な利用ならfalseを返す
	 * @param customer
	 * @param suica
	 * @return
	 */
	public boolean acceptTicket1(Customer customer, Suica suica) {
		if(suica.isInsideOfGate()) {
			return false;
		}
		suica.setInsideOfGate(true);
		suica.setStation(this);
		return true;
	}
	
	/**
	 * 電車から降りた後改札を通す
	 * 料金が不足しているとfalseを返す
	 * @param customer
	 * @param ticket
	 * @return
	 */
	public boolean acceptTicket2(Customer customer, Ticket ticket) {
		if(ticket instanceof ChildTicket) {
			if(Price.price(ticket.getStation(), this) <= ticket.getPrice() * 2) {
				return true;
			} 
		}
			
		if(Price.price(ticket.getStation(), this) <= ticket.getPrice()) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 電車から降りた後改札を通す
	 * Suicaの残高が不足しているとfalseを返す
	 * @param customer
	 * @param suica
	 * @return
	 */
	public boolean acceptTicket2(Customer customer, Suica suica) {
		if(Price.price(suica.getStation(), this) <= suica.getBalance()) {
			suica.pay(Price.price(suica.getStation(), this));
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 切符を販売する
	 * この時点では大人が子供料金で買っても問題ない
	 * @param price
	 * @param isChild
	 * @return
	 */
	public Ticket sellTicket(int price, boolean isChild) {
		if(isChild) {
			return new ChildTicket(price);
		} else {
			return new Ticket(price);
		}
	}
}
