package train;

/**
 * 料金を計算する
 * @author Hashimoto
 *
 */
public class Price {
	public static int price(Station from, Station to) {
		int price;
		if(from.getPlace() == Station.HAMAMATSU && to.getPlace() == Station.KAKEGAWA) {
			price = 500;
		} else if(from.getPlace() == Station.HAMAMATSU && to.getPlace() == Station.SHIZUOKA) {
			price = 1320;
		} else if(from.getPlace() == Station.KAKEGAWA && to.getPlace() == Station.HAMAMATSU) {
			price = 500;
		} else if(from.getPlace() == Station.KAKEGAWA && to.getPlace() == Station.SHIZUOKA) {
			price = 840;
		} else if(from.getPlace() == Station.SHIZUOKA && to.getPlace() == Station.KAKEGAWA) {
			price = 840;
		} else if(from.getPlace() == Station.SHIZUOKA && to.getPlace() == Station.HAMAMATSU) {
			price = 1320;
		} else {
			price = -1; //ここには来ないはず
		}
		return price;
	}
}
