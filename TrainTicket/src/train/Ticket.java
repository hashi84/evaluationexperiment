package train;

public class Ticket {
	private int price;
	private Station station; //乗車駅
	private boolean isInsideOfGate = false; //改札内部にいるかどうか

	public Ticket(int price) {
		this.price = price;
	}
	
	/**
	 * 乗車駅を設定する
	 * @param station
	 */
	public void setStation(Station station) {
		this.station = station;
		isInsideOfGate = true;
	}
	
	/**
	 * 乗車駅を返す
	 * @return
	 */
	public Station getStation() {
		return station;
	}
	
	public boolean isInsideOfGate() {
		return isInsideOfGate;
	}
	
	public void setInsideOfGate(boolean b) {
		this.isInsideOfGate = b;
	}
	
	public int getPrice() {
		return price;
	}
}
