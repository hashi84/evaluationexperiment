package train;

public class Customer {
	private int age;
	private Ticket ticket;
	private Suica suica;
	
	public Customer(int age) {
		this.age = age;
	}
	
	public Customer(int age, Suica suica) {
		this.suica = suica;
	}

	/**
	 * 切符を買う
	 * isChildがtrueならば子供料金で買うが、
	 * 実際の年齢はここでは考慮しない
	 * @param station
	 * @param price
	 * @param isChild
	 */
	public void buyTicket(Station station, int price, boolean isChild) {
		ticket = station.sellTicket(price, isChild);
	}
	
	/**
	 * 改札を通って電車に乗る
	 * 改札を通ることができればtrueを返す
	 * @param station
	 * @return
	 */
	public boolean rideTrain(Station station) {
		if(suica == null) {
			return station.acceptTicket1(this, ticket);
		} else {
			return station.acceptTicket1(this, suica);
		}
	}
	
	/**
	 * 電車を降りて改札を通る
	 * 改札を通ることができればtrueを返す
	 * @param station
	 * @return
	 */
	public boolean getOffTrain(Station station) {
		if(suica == null) {
			return station.acceptTicket2(this, ticket);
		} else {
			return station.acceptTicket2(this, suica);
		}
	}
	
	public int getAge() {
		return age;
	}
}
