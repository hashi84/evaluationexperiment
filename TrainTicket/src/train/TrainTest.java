package train;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TrainTest {
	private Station hamamatsu;
	private Station kakegawa;
	private Station shizuoka;
	
	private Customer taro;
	private Customer jiro;
	private Customer saburo;

	/**
	 * 必要分の料金で改札を通れることをテストする
	 */
	@Test
	public void testPrice() {
		taro.buyTicket(hamamatsu, 1320, false);
		assertTrue(taro.rideTrain(hamamatsu));
		assertTrue(taro.getOffTrain(shizuoka));
		
		taro.buyTicket(shizuoka, 840, false);
		assertTrue(taro.rideTrain(shizuoka));
		assertTrue(taro.getOffTrain(kakegawa));
	}
	
	/**
	 * 12歳未満だけが子供料金で乗れる
	 */
	@Test
	public void testChildPrice() {
		taro.buyTicket(hamamatsu, 660, true);
		jiro.buyTicket(hamamatsu, 660, true);
		saburo.buyTicket(hamamatsu, 660, true);
		
		assertFalse(taro.rideTrain(hamamatsu));
		assertFalse(jiro.rideTrain(hamamatsu));
		assertTrue(saburo.rideTrain(hamamatsu)); //三郎だけは子供料金で乗れる
		
		assertTrue(saburo.getOffTrain(shizuoka));
	}
	
	/**
	 * Suicaで改札を通る
	 */
	@Test
	public void testSuica() {
		Suica suica = new Suica(2000); //2000円チャージ済みのSuica
		taro = new Customer(13, suica);
		
		taro.rideTrain(shizuoka);
		taro.getOffTrain(hamamatsu);
		assertEquals(680, suica.getBalance()); //1320円払ったので残高は680円
	}

	/**
	 * 各テストが実行される前に 変数の初期化
	 */
	@Before
	public void init() {
		hamamatsu = new Station(Station.HAMAMATSU);
		kakegawa = new Station(Station.KAKEGAWA);
		shizuoka = new Station(Station.SHIZUOKA);
		
		taro = new Customer(13);
		jiro = new Customer(12);
		saburo = new Customer(11);
		
		
	}
}
