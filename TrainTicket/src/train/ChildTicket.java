package train;

/**
 * 子供料金の切符
 * @author Hashimoto
 *
 */
public class ChildTicket extends Ticket {

	public ChildTicket(int price) {
		super(price);
	}
	
}
