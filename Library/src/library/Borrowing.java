package library;
/**
 * 本の貸出を表現するクラス
 * @author Hashimoto
 *
 */
public class Borrowing {
	private User user;
	private Book book;
	private Date loanPeriod; //返却期限
	private boolean hasReturned;
	private Library library;
	
	Borrowing(User user, Book book, Date loanPeriod, Library library) {
		this.user = user;
		this.book = book;
		this.loanPeriod = loanPeriod;
		this.hasReturned = false;
		this.library = library;
	}
	
	/**
	 * 返却期限が切れていればtrueを返す
	 * @return
	 */
	boolean hasExcessed() {
		return loanPeriod.compareTo(Date.today) < 0;
	}
	
	Book getBook() {
		return book;
	}
	
	Date getLoanPeriod() {
		return loanPeriod;
	}
	
	void setReturned(boolean hasReturned) {
		this.hasReturned = hasReturned;
	}
	
	boolean hasReturned() {
		return hasReturned;
	}
	
	public Library getLibrary() {
		return library;
	}
}
