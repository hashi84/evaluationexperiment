package library;

import java.util.*;

/**
 * 図書館を表すクラス
 * 利用者に本を貸したり返してもらったりする
 * @author Hashimoto
 *
 */
public class Library {
	static final int LIMIT_NUMBER_OF_BOOKS = 5;
	private List<Book> lendableBooks; //貸し出せる本
	private List<Date> closedDays; //休館日
	
	public Library(List<Book> books) {
		this.lendableBooks = books;
		closedDays = new ArrayList<>();
		closedDays.add(new Date(1, 1));
		closedDays.add(new Date(1, 2));
	}
	
	/**
	 * 本を貸してその貸出のBorrowingインスタンスを返す
	 * ただし、利用者が延滞していたり本が既に貸出中であれば貸し出せず、nullを返す
	 * 借りられる期間は本の種類によって変わる
	 * @param user
	 * @param book
	 * @return
	 */
	public Borrowing lend(User user, Book book) {
		Borrowing b = null;

		if(user.numOfBorrowingBooks() >= LIMIT_NUMBER_OF_BOOKS) {
			return null;
		}
		
		if(user.hasExcessed()) {
			return null;
		}
		
		if(isClosed()) {
			return null;
		}
				
		if(book instanceof Novel && lendableBooks.contains(book) && !user.hasExcessed()) {
			b =  new Borrowing(user, book, Date.today.plus(7), this);
		} else if(book instanceof Magazine && lendableBooks.contains(book) && !user.hasExcessed()) {
			b =  new Borrowing(user, book, Date.today.plus(3), this);
		} else if(book instanceof TechnicalBook && lendableBooks.contains(book) && !user.hasExcessed()) {
			b = new Borrowing(user, book, Date.today.plus(14), this);
		} else if(book instanceof Comic && lendableBooks.contains(book) && !user.hasExcessed()) {
			b = new Borrowing(user, book, Date.today.plus(7), this);
		}
		
		if(b != null) {
			lendableBooks.remove(book);
		}

		return b;
	}
	
	/**
	 * 休館日ならtrueを返す
	 * @return
	 */
	public boolean isClosed() {
		for(int i = 0; i < closedDays.size(); i++) {
			if(closedDays.get(i).equals(Date.today)) {
				return true;
			}
		}
		return false;
	}
	
	public List<Date> getClosedDays() {
		return closedDays;
	}
	
	/**
	 * 新しい本の入荷
	 * @param book
	 */
	public void addBook(Book book) {
		lendableBooks.add(book);
	}
	
	/**
	 * 新しい本の入荷
	 * @param books
	 */
	public void addBook(Collection<Book> books) {
		for(Book book : books) {
			addBook(book);
		}
	}
	
	/**
	 * 本の返却を受け付ける
	 * @param user
	 * @param book
	 * @param borrowing
	 */
	public void returnBook(User user, Book book, Borrowing borrowing) {
		borrowing.setReturned(true);
		lendableBooks.add(book);
	}
}
