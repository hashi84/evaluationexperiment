package library;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DateTest {

	/**
	 * Date同士の減算をテスト
	 */
	@Test
	public void testSub() {
		assertEquals(new Date(8, 27).sub(new Date(8, 11)), 16);
		assertEquals(new Date(8, 2).sub(new Date(7, 25)), 8);
	}
	/**
	 * n日後を求めるテスト
	 * すべての月に31日まであることにしているので要注意
	 */
	@Test
	public void testPlus() {
		assertEquals(3, new Date(9, 3).getDay());
		assertEquals(17, new Date(8, 12).plus(5).getDay());
		assertEquals(2, new Date(9, 2).plus(62).getDay());
	}

	/**
	 * 同じ日付なら別のインスタンスでもequals()がtrueを返すことをテスト
	 */
	@Test
	public void testEquals() {
		assertEquals(new Date(8, 15), new Date(8, 15));
		
		Date.today = new Date(9, 2);
		assertEquals(new Date(9, 2), Date.today);
	}
}
