package library;

import java.util.ArrayList;
import java.util.List;

/**
 * 図書館の利用者
 * @author Hashimoto
 *
 */
public class User {
	private String name;
	private List<Book> books = new ArrayList<>();
	private List<Borrowing> borrowings = new ArrayList<>();
	
	public User(String name) {
		this.name = name;
	}
	
	/**
	 * 本を延滞していればtrueを返す
	 * @return
	 */
	boolean hasExcessed() {
		for(Borrowing borrowing : borrowings) {
			if(borrowing.hasExcessed() && !borrowing.hasReturned()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 本を借りる
	 * @param library
	 * @param book
	 */
	public void borrow(Library library, Book book) {
		Borrowing b = library.lend(this, book);
		if(b != null) {
			borrowings.add(b);
			books.add(book);
		}
	}
	
	/**
	 * 本を返却する
	 * @param borrowing
	 */
	public void returnBook(Borrowing borrowing) {
		borrowing.getLibrary().returnBook(this, borrowing.getBook(), borrowing);
		books.remove(borrowing.getBook());
	}
	
	/**
	 * 借りている本の数を返す
	 * @return
	 */
	int numOfBorrowingBooks() {
		return books.size();
	}
	
	public List<Borrowing> getBorrowings() {
		return borrowings;
	}
	
	public List<Book> getBooks() {
		return books;
	}
}
