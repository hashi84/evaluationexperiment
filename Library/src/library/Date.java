package library;

/**
 * 日付を表すクラス
 * 簡略化のためにすべての月が31日まであることにしている
 * @author Hashimoto
 *
 */
public class Date implements Comparable<Date>{
	public static Date today = new Date(9, 12); //今日の日付
	
	private int month;
	private int day;
	
	public Date(int month, int day) {
		this.month = month;
		this.day = day;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getDay() {
		return day;
	}

	/**
	 * 自身の日付が引数の日付よりも後なら1を、
	 * 前なら-1を、同じ日付なら0を返す
	 */
	@Override
	public int compareTo(Date arg0) {
		if(month == arg0.month && day == arg0.day) { 
			return 0;
		} else if(month > arg0.month) {
			return 1;
		} else if(month < arg0.month) {
			return -1;
		} else {
			if(day > arg0.day) {
				return 1;
			} else if(day <arg0.day) {
				return -1;
			}
		}
		return 0;
	}
	
	/**
	 * n日後の日付を返す
	 * @param day
	 * @return
	 */
	public Date plus(int day) {
		if(this.day + day <= 31)
			return new Date(month, day + this.day);
		else
			return new Date(month + (this.day + day) % 31, (this.day + day) / 31);
	}
	
	/**
	 * 日付同士の差分を日数で返す
	 * すべての月が31日まであることに注意
	 * @param date
	 * @return
	 */
	public int sub(Date date) {
		if(month == date.month) {
			return day - date.day;
		} else {
			return (month - date.month) * 31 + day - date.day;
		}
	}
	
	/**
	 * 日付が同じならtrueを返す
	 */
	@Override
	public boolean equals(Object o) {
		Date d;
		if(o instanceof Date) {
			d = (Date)o;
			if(d.getMonth() == month && d.getDay() == day) {
				return true;
			}
		}
		return false;
	}
}
