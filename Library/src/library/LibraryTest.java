package library;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import library.*;

public class LibraryTest {
	private Library library;
	
	private User ichiro = new User("一郎");
	private User jiro = new User("二郎");
	private User saburo = new User("三郎");
	
	private Book magazine1 = new Magazine("月刊〇〇1月号", "〇〇社", Magazine.MONTHLY, 1);
	private Book magazine2 = new Magazine("月刊〇〇2月号", "〇〇社", Magazine.MONTHLY, 2);
	private Book novel1 = new Novel("山羊の足跡", "山田太郎");
	private Book tech1 = new TechnicalBook("Effective Java", "Joshua Bloch");
	
	private List<Book> weeklyMagazines = new ArrayList<>();

	/**
	 * 貸出中でない本は借りられることをテスト
	 */
	@Test
	public void testBorrowing() {
		ichiro.borrow(library, novel1);
		ichiro.borrow(library, magazine1);
		
		assertEquals(2, ichiro.getBooks().size());
		
		jiro.borrow(library, magazine1); //この本は一郎が借りている
		
		assertEquals(0, jiro.getBooks().size());
	}
	
	/**
	 * 5冊以上は借りられないことをテスト
	 */
	@Test
	public void testOverBorrowing() {
		for(Book magazine : weeklyMagazines) {
			saburo.borrow(library, magazine);
		}
		
		assertEquals(5, saburo.getBooks().size());
		
		saburo.borrow(library, magazine2); //6冊目
		
		assertEquals(5, saburo.getBooks().size());
		
	}
	
	/**
	 * 新しく入荷された本が借りられるかのテスト
	 */
	@Test
	public void testNewBookBorrowing() {
		jiro.borrow(library, tech1);
		assertEquals(0, jiro.getBooks().size());
		
		library.addBook(tech1);
		jiro.borrow(library, tech1);
		
		assertEquals(1, jiro.getBooks().size());
	}
	
	/**
	 * 返却期限が切れている本を返すまでは新しく借りられない
	 */
	@Test
	public void testExcessed() {
		Date.today = new Date(9, 2);
		ichiro.borrow(library, magazine1);
		assertEquals(1, ichiro.getBooks().size());
		
		Date.today = new Date(10, 2); //1ヶ月後
		ichiro.borrow(library, magazine2);
		assertEquals(1, ichiro.getBooks().size());
		
		for(Borrowing borrowing : ichiro.getBorrowings()) {
			ichiro.returnBook(borrowing); //全部返す
		}
		
		ichiro.borrow(library, magazine2);
		assertEquals(1, ichiro.getBooks().size());
	}
	
	/**
	 * 各テストの前に呼ばれ、変数の初期化などをする
	 * テストとテストの間に初期化されるので、あるテストの結果が他のテストに影響することはない
	 */
	@Before
	public void init() {
		List<Book> books = new ArrayList<>();
		books.add(magazine1);
		books.add(magazine2);
		books.add(novel1);
		
		for(int i = 0; i < 5; i++) {
			weeklyMagazines.add(new Magazine("週間××" + (i+1) + "号", "××社", Magazine.WEEKLY, i+1));
		}
		books.addAll(weeklyMagazines);
		
		library = new Library(books);
	}
}
