package library;

public class Magazine extends Book {
	private int volume;
	private int interval;
	public static int WEEKLY = 0;
	public static int MONTHLY = 1;
	public static int ANNUAL = 2;
	
	public Magazine(String title, String author, int interval, int volume) {
		super(title, author);
		this.volume = volume;
		this.interval = interval;
	}
}
