package store;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class StoreTest {
	private ConvinienceStore store;
	private Customer customer;
	private Item magazine = new Magazine(200, 150);
	private Item tea = new Tea(128, 100);
	private Item postCard = new PostCard(50, 50);
	
	/**
	 * 変数の初期化
	 * 各テストが実行される前に呼ばれる
	 */
	@Before
	public void init() {
		List<Item> items = new ArrayList<>();
		items.add(new Magazine(200, 100));
		store = new ConvinienceStore(items);
		
		customer = new Customer(new Cash(1000));
	}
	
	/**
	 * 買い物をしたあとの残金や売上が正しく計算できるかのテスト
	 */
	@Test
	public void testSell() {
		customer.buy(magazine, store);
		customer.buy(tea, store);
		
		assertEquals(672, customer.getCashAmount());
		assertEquals(2, customer.getBroughtItems().size());
		
		assertEquals(78, store.getEarnings());
	}
	
	/**
	 * クレジットカードで支払うテスト
	 * ハガキはクレジットカードで買えない
	 */
	@Test
	public void testCreditCard() {
		customer = new Customer(new CreditCard());
		customer.buy(postCard, store);
		
		assertEquals(0, customer.getBroughtItems().size());
		
		customer.buy(tea, store);
		assertEquals(1, customer.getBroughtItems().size());
	}
	
	/**
	 * クレジットカードの限度額をテストする
	 */
	@Test
	public void testCreditLimit() {
		Item expensiveTea = new Tea(128000, 100000); //すごく高いお茶
		List<Item> items = new ArrayList<>();
		items.add(expensiveTea);
		items.add(expensiveTea);
		
		store = new ConvinienceStore(items);
		
		customer = new Customer(new CreditCard());
		customer.buy(expensiveTea, store);
		customer.buy(expensiveTea, store); //1つ買った時点で限度額を超えているので2つ目は買えない
		
		assertEquals(1, customer.getBroughtItems().size());
	}
}
