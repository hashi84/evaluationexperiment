package store;

import java.util.List;

public class ConvinienceStore {
	private int earnings = 0; //利益の合計
	private List<Item> items; //商品
	
	public ConvinienceStore(List<Item> items) {
		this.items = items;
	}
	
	/**
	 * 現金による会計
	 * 会計が済むと商品を返す
	 * @param item
	 * @param cash
	 * @return
	 */
	public Item sell(Item item, Cash cash) {
		int price = item.getPrice();
		if(!cash.canPay(price)) {
			return null;
		}
		cash.pay(price);
		items.remove(item);
		earnings += item.profit();
		return item;
	}
	
	/**
	 * クレジットカードによる会計
	 * カードが止められていたり限度額を超えていると払えない
	 * また、ハガキはカード払いができない
	 * 会計が済むと商品を返す
	 * @param item
	 * @param card
	 * @return
	 */
	public Item sell(Item item, CreditCard card) {
		if(item instanceof PostCard) {
			return null;
		}
		if(card.getUsingExpense() > CreditCard.CREDIT_LIMIT) {
			return null;
		}
		if(!card.isAvailable()) {
			return null;
		}
		
		int price = item.getPrice();
		card.pay(price);
		items.remove(item);
		earnings += item.profit();
		return item;
	}
	
	/**
	 * 現金による支払い
	 * ポイントカードを持っているのでポイントが付く
	 * @param item
	 * @param cash
	 * @param pointCard
	 * @return
	 */
	public Item sell(Item item, Cash cash, PointCard pointCard) {
		Item broughtItem = sell(item, cash);
		if(broughtItem != null) {
			pointCard.addPoint(broughtItem);
		}
		return broughtItem;
	}
	
	/**
	 * クレジットカードによる支払い
	 * ポイントカードを持っているのでポイントが付く
	 * @param item
	 * @param creditCard
	 * @param pointCard
	 * @return
	 */
	public Item sell(Item item, CreditCard creditCard, PointCard pointCard) {
		Item broughtItem = sell(item, creditCard);
		if(broughtItem != null) {
			pointCard.addPoint(broughtItem);
		}
		return broughtItem;
	}
	
	public int getEarnings() {
		return earnings;
	}
}
