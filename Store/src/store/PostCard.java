package store;

/**
 * ハガキのクラス
 * ハガキはクレジットカードによる支払いができない
 * @author Hashimoto
 *
 */
public class PostCard extends Item {

	PostCard(int price, int cost) {
		super(price, cost);
	}

}
