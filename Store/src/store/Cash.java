package store;

/**
 * 現金のクラス
 * @author Hashimoto
 *
 */
public class Cash extends Money {
	private int amount;
	
	public Cash(int amount) {
		this.amount = amount;
	}
	
	/**
	 * 現金で支払う
	 * @param amount
	 */
	public void pay(int amount) {
		this.amount -= amount;
	}
	
	public boolean canPay(int price) {
		return price <= amount;
	}
	
	public int getAmount() {
		return amount;
	}
}
