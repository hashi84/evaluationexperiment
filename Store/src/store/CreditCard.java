package store;

public class CreditCard extends Money {
	private int usingExpense;
	private boolean isAvailable = true; //利用可能かどうか
	public static final int CREDIT_LIMIT = 50000; //限度額
	
	public void pay(int price) {
		usingExpense += price;
	}
	
	public int getUsingExpense() {
		return usingExpense;
	}
	
	public boolean isAvailable() {
		return isAvailable;
	}

	/**
	 * カードの利用を停止する
	 */
	public void invalidate() {
		isAvailable = false;
	}
}
