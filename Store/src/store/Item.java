package store;

/**
 * 商品のクラス
 * @author Hashimoto
 *
 */
public abstract class Item {
	protected int price; //売値
	protected int cost; //仕入れ値
	
	Item(int price, int cost) {
		this.price = price;
		this.cost = cost;
	}
	
	public int profit() {
		return price - cost;
	}
	
	public int getPrice() {
		return price;
	}
}
