package store;

/**
 * ポイントカードのクラス
 * 支払いの際に提示すると1%のポイントが貯まる
 * @author Hashimoto
 *
 */
public class PointCard {
	private int point;
	
	public PointCard(int point) {
		this.point = point;
	}
	
	public int getPoint() {
		return point;
	}
	
	public void addPoint(Item item) {
		point += item.getPrice() / 100;
	}
}
