package store;

import java.util.ArrayList;
import java.util.List;

public class Customer {
	private Cash cash;
	private CreditCard card;
	
	private List<Item> broughtItems = new ArrayList<>(); //買った商品
	
	public Customer(Cash cash) {
		this.cash = cash;
	}
	
	public Customer(CreditCard card) {
		this.card = card;
	}
	
	public Customer(Cash cash, CreditCard card) {
		this.cash = cash;
		this.card = card;
	}
	
	/**
	 * 商品を買う(ポイントカードを持っていない)
	 * クレジットカードがあればカードで、なければ現金で払う
	 * @param item
	 * @param store
	 */
	public void buy(Item item, ConvinienceStore store) {
		Item broughtItem = null;
		if(cash != null && card != null) {
			broughtItem = store.sell(item, card);
		} else if(card != null) {
			broughtItem = store.sell(item, card);
		} else if(cash != null && cash.getAmount() >= item.getPrice()) {
			broughtItem = store.sell(item, cash);
		} else if(cash != null && cash.getAmount() < item.getPrice()) {
			return; //お金が足りなかった
		}
		
		if(broughtItem != null) {
			broughtItems.add(broughtItem);
		}
	}
	
	/**
	 * 商品を買う(ポイントカードを持っている)
	 * @param item
	 * @param store
	 * @param pointCard
	 */
	public void buy(Item item, ConvinienceStore store, PointCard pointCard) {
		Item broughtItem = null;
		if(pointCard == null) {
			buy(item, store);
			return;
		}
		
		if(cash != null && card != null) {
			broughtItem = store.sell(item, card, pointCard);
		} else if(card != null) {
			broughtItem = store.sell(item, card, pointCard);
		} else if(cash != null && cash.getAmount() >= item.getPrice()) {
			broughtItem = store.sell(item, cash, pointCard);
		} else if(cash != null && cash.getAmount() < item.getPrice()) {
			return;
		}
		
		if(broughtItem != null) {
			broughtItems.add(broughtItem);
		}
	}
	
	/**
	 * 持っている現金の金額を返す
	 * @return
	 */
	public int getCashAmount() {
		if(cash == null) return 0;
		else return cash.getAmount();
	}
	
	public List<Item> getBroughtItems() {
		return broughtItems;
	}
}
